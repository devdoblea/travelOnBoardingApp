export const onboarding1 = require('../assets/images/ev1444_1.jpg');
export const onboarding2 = require('../assets/images/ev1772fp_01.jpg');
export const onboarding3 = require('../assets/images/ev1774fp_01.jpg');

export default {
  onboarding1,
  onboarding2,
  onboarding3,
};
